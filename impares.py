
# pedimos los numeros
numero1 = int(input("Dame un número entero no negativo: "))
numero2 = int(input("Dame otro: "))
# Asegurarse de que num1 sea menor o igual que num2
if numero1 > numero2:
    numero1, numero2 = numero2, numero1

# Calcular la suma de los números impares entre num1 y num2
suma_impares = 0
for i in range(numero1, numero2 + 1):
    if i % 2 != 0:  # Verificar si el número es impar
        suma_impares += i

# Mostrar el resultado
print(f"La suma de los números impares entre {numero1} y {numero2} es: {suma_impares}")